FROM java:8
WORKDIR /
ADD server-udp.jar server-udp.jar
EXPOSE 4445
CMD java -jar server-udp.jar