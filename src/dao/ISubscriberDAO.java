package dao;

import java.util.Set;

import domain.Subscriber;

public interface ISubscriberDAO {

	Subscriber getSubscriber(int id);
	Subscriber getSubscriberByUclvIdAndCarPlate(String uclvId, String carPlate);
	Set<Subscriber> getSubscriberByUclvId(String uclvId);
	Set<Subscriber> getSubscriberByCarPlate(String carPlate);
	boolean insertSubscriber(Subscriber subscriber);
	boolean updateSubscriber(Subscriber subscriber);
	boolean deleteSubscriber(int id);
}
