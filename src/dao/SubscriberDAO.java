package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import domain.Register;
import domain.Subscriber;
import factory.ConnectionFactory;

public class SubscriberDAO implements ISubscriberDAO {

	@Override
	public Subscriber getSubscriber(int id) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM subscriber WHERE id=" + id);
			if (rs.next()) {
				System.out.println("Got Subscriber with id: " + id);
				return extractSubscriberFromResultSet(rs);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public Subscriber getSubscriberByUclvIdAndCarPlate(String uclvId, String carPlate) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			PreparedStatement ps = connection
					.prepareStatement("SELECT * FROM subscriber WHERE uclv_id=? AND car_plate=?");
			ps.setString(1, uclvId);
			ps.setString(2, carPlate);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				System.out.println("Got Subscriber with CarPlate: " + carPlate + " and uclv: " + uclvId);
				return extractSubscriberFromResultSet(rs);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean insertSubscriber(Subscriber subscriber) {
		Connection connection = ConnectionFactory.getConnection();
		/*if(getSubscriberByUclvId(subscriber.getUclvId()) != null) {
			System.out.println("Cannot create subscriber because uclv_id: " + subscriber.getUclvId() + " already exists.");
			return false;
		} else if(getSubscriberByCarPlate(subscriber.getCarPlate()) != null) {
			System.out.println("Cannot create subscriber because carPlate: " + subscriber.getCarPlate() + " already exists.");
			return false;
		}*/
		try {
			PreparedStatement ps = connection.prepareStatement("INSERT INTO subscriber VALUES (NULL, ?, ?)");
			ps.setString(1, subscriber.getUclvId());
			ps.setString(2, subscriber.getCarPlate());
			int i = ps.executeUpdate();
			System.out.println("Created Subscriber with CarPlate: " + subscriber.getCarPlate());
			
			return i == 1;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean updateSubscriber(Subscriber subscriber) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			PreparedStatement ps = connection
					.prepareStatement("UPDATE subscriber SET uclv_id=?, car_plate=? WHERE id=?");
			ps.setString(1, subscriber.getUclvId());
			ps.setString(2, subscriber.getCarPlate());
			ps.setInt(3, subscriber.getId());
			int i = ps.executeUpdate();
			System.out.println("Updated Subscriber with CarPlate: " + subscriber.getCarPlate());
			
			return i == 1;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteSubscriber(int id) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			Statement stmt = connection.createStatement();
			int i = stmt.executeUpdate("DELETE FROM subscriber WHERE id=" + id);
			System.out.println("Deleted Subscriber with id: " + id);
			
			return i == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	private Subscriber extractSubscriberFromResultSet(ResultSet rs) throws SQLException {
		Subscriber subscriber = new Subscriber();
		subscriber.setId(rs.getInt("id"));
		subscriber.setUclvId(rs.getString("uclv_id"));
		subscriber.setCarPlate(rs.getString("car_plate"));
		return subscriber;
	}

	@Override
	public Set<Subscriber> getSubscriberByUclvId(String uclvId) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			PreparedStatement ps = connection
					.prepareStatement("SELECT * FROM subscriber WHERE uclv_id=?");
			ps.setString(1, uclvId);
			ResultSet rs = ps.executeQuery();
			
			Set<Subscriber> subscribers = new HashSet<Subscriber>();
			while (rs.next()) {
				Subscriber subscriber = extractSubscriberFromResultSet(rs);
				subscribers.add(subscriber);
			}
			System.out.println("Got Subscribers with uclvId: " + uclvId);
			return subscribers;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public Set<Subscriber> getSubscriberByCarPlate(String carPlate) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			PreparedStatement ps = connection
					.prepareStatement("SELECT * FROM subscriber WHERE car_plate=?");
			ps.setString(1, carPlate);
			ResultSet rs = ps.executeQuery();
			
			Set<Subscriber> subscribers = new HashSet<Subscriber>();
			while (rs.next()) {
				Subscriber subscriber = extractSubscriberFromResultSet(rs);
				subscribers.add(subscriber);
			}
			System.out.println("Got Subscribers with carPlate: " + carPlate);
			return subscribers;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
