package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import domain.Register;
import factory.ConnectionFactory;

public class RegisterDAO implements IRegisterDAO {

	@Override
	public Register getRegister(int id) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM register WHERE id=" + id);
			if (rs.next()) {
				System.out.println("Got Register with id: " + id);
				return extractRegisterFromResultSet(rs);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public Set<Register> getRegistersBySubscriberId(int subscriberId) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM register WHERE subscriber_id=?");
			ps.setInt(1, subscriberId);
			ResultSet rs = ps.executeQuery();

			Set<Register> registers = new HashSet<Register>();
			while (rs.next()) {
				Register register = extractRegisterFromResultSet(rs);
				registers.add(register);
			}
			System.out.println("Got Registers of subscriber: " + subscriberId);
			return registers;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	@Override
	public List<Register> getRegistersWithPriority() {
		Connection connection = ConnectionFactory.getConnection();
		try {
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM register WHERE priority != -1 ORDER BY priority");
			ResultSet rs = ps.executeQuery();

			List<Register> registers = new LinkedList<Register>();
			while (rs.next()) {
				Register register = extractRegisterFromResultSet(rs);
				registers.add(register);
			}
			System.out.println("Got Priority Registers");
			return registers;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	@Override
	public Register getPrioritizedBySubscriberId(int subscriberId) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM register WHERE (subscriber_id=? AND priority != -1)");
			ps.setInt(1, subscriberId);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				System.out.println("Got Prioritized Register of subscriberId: " + subscriberId);
				return extractRegisterFromResultSet(rs);
			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean insertRegister(Register register) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			PreparedStatement ps = connection.prepareStatement("INSERT INTO register VALUES (NULL, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, register.getSubscriberId());
			ps.setDate(2, register.getDate());
			ps.setString(3, register.getTime());
			ps.setInt(4, register.getFlag());
			ps.setFloat(5, register.getCharge());
			ps.setInt(6, register.getPriority());
			int i = ps.executeUpdate();
			System.out.println("Created Register of subscriber: " + register.getSubscriberId());

			if(i == 0) {
				System.out.println("Creting Register failed, no rows affected.");
			}
			
			try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					register.setId(generatedKeys.getInt(1));
				} else {
					System.out.println("Creating Register failed, no ID obtained.");
				}
			}
			return i == 1;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean updateRegister(Register register) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			PreparedStatement ps = connection.prepareStatement("UPDATE register SET subscriber_id=?, date=?, time=?, flag=?, charge=?, priority=? WHERE id=?");
			ps.setInt(1, register.getSubscriberId());
			ps.setDate(2, register.getDate());
			ps.setString(3, register.getTime());
			ps.setInt(4, register.getFlag());
			ps.setFloat(5, register.getCharge());
			ps.setInt(6, register.getPriority());
			ps.setInt(7, register.getId());
			int i = ps.executeUpdate();
			System.out.println("Updated Register of subscriber: " + register.getSubscriberId());

			return i == 1;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteRegister(int id) {
		Connection connection = ConnectionFactory.getConnection();
		try {
			Statement stmt = connection.createStatement();
			int i = stmt.executeUpdate("DELETE FROM register WHERE id=" + id);
			System.out.println("Deleted Register with id: " + id);
			
			return i == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	private Register extractRegisterFromResultSet(ResultSet rs) throws SQLException {
		Register register = new Register();
		register.setId(rs.getInt("id"));
		register.setSubscriberId(rs.getInt("subscriber_id"));
		register.setDate(rs.getDate("date"));
		register.setTime(rs.getString("time"));
		register.setFlag(rs.getInt("flag"));
		register.setCharge(rs.getInt("charge"));
		register.setPriority(rs.getInt("priority"));
		return register;
	}

}
