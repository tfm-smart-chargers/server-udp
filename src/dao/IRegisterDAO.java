package dao;

import java.util.List;
import java.util.Set;

import domain.Register;

public interface IRegisterDAO {

	Register getRegister(int id);
	Set<Register> getRegistersBySubscriberId(int subscriberId);
	List<Register> getRegistersWithPriority();
	Register getPrioritizedBySubscriberId(int subscriberId);
	boolean insertRegister(Register register);
	boolean updateRegister(Register register);
	boolean deleteRegister(int id);
}
