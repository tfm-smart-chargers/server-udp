package main;

public class StartClient {
	
	public static void main(String[] args) {
		System.out.println("Starting client...");
		EchoClient client = new EchoClient();
		
		client.sendEcho("1#0000CCC#0#-1");
		client.sendEcho("2#0000CCC#2#30");
		
		client.sendEcho("1#1111DDD#0#-1");
		client.sendEcho("3#1111DDD#2#20");
		
		client.sendEcho("1#2222FFF#0#-1");
		client.sendEcho("4#2222FFF#2#50");
		
		client.sendEcho("1#3333YYY#0#-1");
		client.sendEcho("4#3333YYY#2#10");
		
		client.sendEcho("1#4444ZZZ#0#-1");
		client.sendEcho("4#4444ZZZ#2#35");
		
		/*String echo = client.sendEcho("hello server");
        echo = client.sendEcho("server is working");
        System.out.println("echo sended");
        
        client.sendEcho("end");
        client.close();
        System.out.println("client closed");*/
	}

}
