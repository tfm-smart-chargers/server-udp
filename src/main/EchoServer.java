package main;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import domain.Register;
import domain.Subscriber;
import service.RegisterService;
import service.SubscriberService;

public class EchoServer extends Thread {
	 
	private static final String SEPARATOR = "#";
	private static final SimpleDateFormat sdf_time = new SimpleDateFormat("HH:mm:ss");
	
    private DatagramSocket socket;
    private boolean running;
    private byte[] buf = new byte[256];
    
    private SubscriberService subscriberService = new SubscriberService();
    private RegisterService registerService = new RegisterService();
 
    public EchoServer() {
        try {
			socket = new DatagramSocket(4445);
			System.out.println("Socket created");
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
 
    public void run() {
        running = true;
        
        System.out.println("Listening on port 4445...");
        System.out.println();
        while (running) {
        	
            DatagramPacket packet 
              = new DatagramPacket(buf, buf.length);
            try {
				socket.receive(packet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
             
            InetAddress address = packet.getAddress();
            int port = packet.getPort();
            packet = new DatagramPacket(buf, buf.length, address, port);
            String received = new String(packet.getData(), 0, packet.getLength());
            
            System.out.println(received);
            
            if(received.split(SEPARATOR).length == 4) {
            	parseMsg(received);
            }
            
            if (received.equals("end")) {
                running = false;
                continue;
            }
            
            Arrays.fill(buf, (byte)0);
        }
        socket.close();
        System.out.println();
        System.out.println("Socket closed");
    }
    
    private boolean parseMsg(String msg) {
    	String data[] = msg.split(SEPARATOR);
    	// Create Subscriber if not exists in BD
    	if(data[0] != "" && data[1] != "" && data[2] != "" && data[3] != "") {
    		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    		
    		String uclvId = data[0];
    		String carPlate = data[1];
    		Date date = new Date(timestamp.getTime());
            String time = sdf_time.format(timestamp).toString();
            Integer flag = Integer.parseInt(data[2]);
            Float charge = Float.valueOf(data[3].trim());
    		
            if(!checkValues(flag, charge)) {
            	System.out.println("Incorrect values:");
            	System.out.println("flag: " + flag + " - charge: " + charge + "\n\n");
            	return false;
            }
            
            boolean subscriberCreated = false;
    		Subscriber subscriber = getSubscriber(uclvId, carPlate);
    		if(subscriber == null) {
    			subscriberCreated = subscriberService.create(new Subscriber(uclvId, carPlate));
    			if(!subscriberCreated) {
    				return false;
    			}
    			subscriber = subscriberService.getByUclvAndPlate(uclvId, carPlate);
    		}
    		
    		Register register = new Register(subscriber.getId(), date, time, flag, charge);
    		registerService.create(register);
    		
    		if(flag == 2) {
    			updatePriorities(register);
    		} else if(flag == 1) {
    			reprioritize(register);
    		}
    		
    		System.out.println("\n");
    		return true;
    	}
    	System.out.println("Incorrect data\n\n");
    	return false;
    }

	private boolean checkValues(Integer flag, Float charge) {
		switch(flag) {
			case 0:
			case 1:
				return charge == -1;
			case 2:
				return charge >= 0 && charge <= 100;
			default: 
				return false;
		}
	}

	private Subscriber getSubscriber(String uclvId, String carPlate) {
    	return subscriberService.getByUclvAndPlate(uclvId, carPlate);
    }
	
	private void updatePriorities(Register register) {
		List<Register> priorRegisters = registerService.getPrioritizedRegisters();
		
		boolean exists = false;
		for(int i = 0; i < priorRegisters.size(); i++) {
			Register reg = priorRegisters.get(i);
			if(register.getSubscriberId() == reg.getSubscriberId()) {
				reg.setPriority(-1);
				registerService.update(reg);
				exists = true;
			}
		}
		
		if(exists) {
			priorRegisters = registerService.getPrioritizedRegisters();
		}
		
		if(priorRegisters.size() == 0) {
			register.setPriority(1000);
			registerService.update(register);
		} 
		
		int pos = -1;
		for(int i = 0; i < priorRegisters.size(); i++) {
			Register reg = priorRegisters.get(i);
			if(register.getCharge() > reg.getCharge()) {
				pos = i - 1;
				register.setPriority(reg.getPriority() - 1);
				registerService.update(register);
				break;
			} else {
				if(i == priorRegisters.size() - 1) {
					pos = i;
					register.setPriority(reg.getPriority());
					registerService.update(register);
					break;
				}
			}
		}
		if(pos != -1) {
			for(int j = pos; j >= 0; j--) {
				Register reg = priorRegisters.get(j);
				reg.setPriority(reg.getPriority() - 1);
				registerService.update(reg);
			}
		}
	}
	
	private void reprioritize(Register register) {
		Subscriber subs = subscriberService.get(register.getSubscriberId());
		
		String carPlate = subs.getCarPlate();

		Set<Subscriber> subscribers = subscriberService.getByCarPlate(carPlate);
		
		for(Subscriber s : subscribers) {
			Register r = registerService.getPrioritizedBySubscriberId(s.getId());
			if(r != null) {
				r.setPriority(-1);
				registerService.update(r);
			}
		}
		
		int priority = 1000;
		List<Register> priorRegisters = registerService.getPrioritizedRegisters();
		for(int i = priorRegisters.size() - 1; i >= 0; i--) {
			Register reg = priorRegisters.get(i);
			if(reg.getPriority() != priority) {
				reg.setPriority(priority);
				registerService.update(reg);
			}
			priority--;
		}
	}
}