package service;

import java.util.List;
import java.util.Set;

import domain.Register;

public interface IRegisterService {

	Register get(int id);
	Set<Register> getBySubscriberId(int subscriberId);
	List<Register> getPrioritizedRegisters();
	Register getPrioritizedBySubscriberId(int subscriberId);
	boolean create(Register register);
	boolean update(Register register);
	boolean delete(int id);
	
}
