package service;

import java.util.Set;

import dao.SubscriberDAO;
import domain.Subscriber;

public class SubscriberService implements ISubscriberService{
	
	private SubscriberDAO subscriberDAO;
	
	public SubscriberService() {
		this.subscriberDAO = new SubscriberDAO();
	}

	@Override
	public Subscriber get(int id) {
		return subscriberDAO.getSubscriber(id);
	}

	@Override
	public Subscriber getByUclvAndPlate(String uclvId, String carPlate) {
		return subscriberDAO.getSubscriberByUclvIdAndCarPlate(uclvId, carPlate);
	}
	
	@Override
	public Set<Subscriber> getByUclv(String uclvId) {
		return subscriberDAO.getSubscriberByUclvId(uclvId);
	}
	
	@Override
	public Set<Subscriber> getByCarPlate(String carPlate) {
		return subscriberDAO.getSubscriberByCarPlate(carPlate);
	}

	@Override
	public boolean create(Subscriber subscriber) {
		return subscriberDAO.insertSubscriber(subscriber);
	}

	@Override
	public boolean update(Subscriber subscriber) {
		return subscriberDAO.updateSubscriber(subscriber);
	}

	@Override
	public boolean delete(int id) {
		return subscriberDAO.deleteSubscriber(id);
	}
	
	
}
