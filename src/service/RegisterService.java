package service;

import java.util.List;
import java.util.Set;

import dao.RegisterDAO;
import domain.Register;

public class RegisterService implements IRegisterService {

	private RegisterDAO registerDAO;
	
	public RegisterService() {
		this.registerDAO = new RegisterDAO();
	}
	
	@Override
	public Register get(int id) {
		return registerDAO.getRegister(id);
	}

	@Override
	public Set<Register> getBySubscriberId(int subscriberId) {
		return registerDAO.getRegistersBySubscriberId(subscriberId);
	}
	
	@Override
	public List<Register> getPrioritizedRegisters() {
		return registerDAO.getRegistersWithPriority();
	}

	@Override
	public Register getPrioritizedBySubscriberId(int subscriberId) {
		return registerDAO.getPrioritizedBySubscriberId(subscriberId);
	}

	@Override
	public boolean create(Register register) {
		return registerDAO.insertRegister(register);
	}

	@Override
	public boolean update(Register register) {
		return registerDAO.updateRegister(register);
	}

	@Override
	public boolean delete(int id) {
		return registerDAO.deleteRegister(id);
	}
}
