package service;

import java.util.Set;

import domain.Subscriber;

public interface ISubscriberService {

	Subscriber get(int id);
	Subscriber getByUclvAndPlate(String uclvId, String carPlate);
	Set<Subscriber> getByUclv(String uclvId);
	Set<Subscriber> getByCarPlate(String carPlate);
	boolean create(Subscriber subscriber);
	boolean update(Subscriber subscriber);
	boolean delete(int id);
	
}
