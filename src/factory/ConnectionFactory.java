package factory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import helper.PropertiesReader;

public class ConnectionFactory {

	/**
	 * Get a connection to database
	 * 
	 * @return Connection object
	 * @throws IOException 
	 */
	public static Connection getConnection() {
		PropertiesReader properties = new PropertiesReader();
		
		String host = "", database = "", username = "", password = "";
		try {
			host = properties.getPropValue("HOST");
			database = properties.getPropValue("DATABASE");
			username = properties.getPropValue("USER");
			password = properties.getPropValue("PASSWORD");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String jdbcUrl = String.format("jdbc:mysql://%s/%s?useSSL=false", host, database);

		try {
			return DriverManager.getConnection(jdbcUrl, username, password);
		} catch (SQLException e) {
			throw new RuntimeException("Error connecting to the database", e);
		}
	}

}
