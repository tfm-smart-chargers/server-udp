package domain;

public class Subscriber {

	private int id;
	private String uclvId;
	private String carPlate;

	public Subscriber() {

	}

	public Subscriber(String uclvId, String carPlate) {
		this.uclvId = uclvId;
		this.carPlate = carPlate;
	}

	public String getUclvId() {
		return uclvId;
	}

	public void setUclvId(String uclvId) {
		this.uclvId = uclvId;
	}

	public String getCarPlate() {
		return carPlate;
	}

	public void setCarPlate(String carPlate) {
		this.carPlate = carPlate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
