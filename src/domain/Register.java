package domain;

import java.sql.Date;

public class Register {

	private int id;
	private int subscriberId;
	private Date date;
	private String time;
	private int flag;
	private float charge;
	private int priority;

	public Register() {
		
	}
	
	public Register(int subscriberId, Date date, String time, int flag, float charge) {
		this.subscriberId = subscriberId;
		this.date = date;
		this.time = time;
		this.flag = flag;
		this.charge = charge;
		this.priority = -1;
	}

	public int getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(int subscriberId) {
		this.subscriberId = subscriberId;
	}

	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public float getCharge() {
		return charge;
	}

	public void setCharge(float charge) {
		this.charge = charge;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "Register [id=" + id + ", subscriberId=" + subscriberId + ", date=" + date + ", time=" + time + ", flag="
				+ flag + ", charge=" + charge + ", priority=" + priority + "]";
	}

}
