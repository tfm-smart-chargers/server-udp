CREATE TABLE `subscriber` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
    `uclv_id` VARCHAR(255) NOT NULL,
    `car_plate` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`));
    
CREATE TABLE `register` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
    `subscriber_id` INT(11) NOT NULL,
    `date` DATE NOT NULL,
    `time` VARCHAR(50) NOT NULL,
    `flag` INT(1) NOT NULL,
    `charge` INT(11),
    `priority` INT(11),
    PRIMARY KEY (`id`),
    KEY `fk_register_subscriber_idx` (`subscriber_id`),
    CONSTRAINT `fk_register_subscriberid` FOREIGN KEY (`subscriber_id`) REFERENCES `subscriber`(`id`));